<?php 
require 'bdd/bddconfig.php';
//Récuperer les 3 variables POST
//sécuriser les variables reçues
$paramOk=false;

if(isset($_POST['nom'])){
    $nom = htmlspecialchars($_POST['nom']);

    if(isset($_POST['descript'])){
        $descript = htmlspecialchars($_POST['descript']);

        if(isset($_POST['refcapteur'])){
            $refcapteur = htmlspecialchars($_POST['refcapteur']);
            $paramOk = true;
        }
    }
}

if($paramOk == true){
// INSERT dans la base
try{
    $objBdd= new PDO("mysql:host=$bddserver;
    dbname=$bddname;
    charset=utf8",$bddlogin,$bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdoStmt =$objBdd->prepare("INSERT INTO bassin (nom, description, refCapteur) VALUES (:nom, :description, :refcapteur)");
    $pdoStmt -> bindParam(':nom',$nom, PDO:: PARAM_STR);
    $pdoStmt -> bindParam(':description',$descript, PDO:: PARAM_STR);
    $pdoStmt -> bindParam(':refcapteur',$refcapteur, PDO:: PARAM_STR);
    $pdoStmt -> execute();

    $lastId=$objBdd->lastInsertId ();

}catch(Exception $prmE){
    die('Erreur:' .$prmE->getMessage());
}

// rediriger automatique vers la page des bassins.php

// header("Location: http://localhost/truites/bassins.php ");

$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'bassins.php';
header("Location: http://$serveur$chemin/$page");

 }else{
     die('Les paramètres reçus ne sont pas valides');
 }

?>