<?php 
require 'bdd/bddconfig.php';
$paramOk=false;

if(isset($_POST['idbassin'])){
    $idbassin= intval(htmlspecialchars($_POST['idbassin']));
    $paramOk = true;
}

if($paramOk== true) {
    try {
         $objBdd= new PDO("mysql:host=$bddserver;
         dbname=$bddname;
         charset=utf8",$bddlogin,$bddpass);
         $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

         $RSlogins =$objBdd->prepare("DELETE FROM temperature WHERE idBassin = :id");
         $RSlogins ->bindParam(':id',$idbassin,PDO::PARAM_INT);
         $RSlogins ->execute();

         $RSlogins =$objBdd->prepare("DELETE FROM bassin WHERE idBassin = :id");
         $RSlogins ->bindParam(':id',$idbassin,PDO::PARAM_INT);
         $RSlogins ->execute();


         $serveur = $_SERVER['HTTP_HOST'];
         $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
         $page = 'bassins.php';
         header("Location: http://$serveur$chemin/$page");
        
        }catch(Exception $prmE){
            die('Erreur:' .$prmE->getMessage());
        }
        
        // rediriger automatique vers la page des bassins.php
        
        // header("Location: http://localhost/truites/bassins.php ");
        
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $page = 'bassins.php';
        header("Location: http://$serveur$chemin/$page");
        
         }else{
             die('Les paramètres reçus ne sont pas valides');
         }
        
?>