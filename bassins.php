<?php $titre = "Les bassins"; ?>
<?php session_start(); ?>
<?php 
require"bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",
   $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE,
   PDO::ERRMODE_EXCEPTION);

   $listeBassins = $objBdd ->query("SELECT * FROM bassin");
   
   } catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
   }
?>
<?php ob_start(); ?>
<article>
    <h1>Les bassins</h1>
    <?php
    while ($unBassin = $listeBassins->fetch()) {
    ?>

    <h2><?php echo $unBassin['nom']; ?></h2>
    <p><?= $unBassin['description']; ?></p>
    <img src="images/<?php echo $unBassin['photo'] ?>" alt="">
    <p><a href="temperature.php?idBassin=<?=$unBassin['idBassin']; ?>&nomBassin=<?=$unBassin['nom']; ?>">Voir les Températures</a></p>

    <?php
    } //fin du while
    $listeBassins->closeCursor(); //libère les ressources de la bdd
    ?>
    
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>